# Singify

The Singify project relies in the use of REST APIs, documentation for the following APIs is available:

* [Spotify](Spotify.md)
* [Facebook](Facebook.md)
* [Singify](Singify.md)

## REST APIs

REST comes from Representational state transfer and is based on resources instead of actions like other architectures.

Actions over the resources are made through requests using the different HTTP verbs available like GET, POST, PUT, DELETE etc. Each of this verb has a different action over a resource defined by the API we are using.

The operation will bring back it's result with a HTTP Code representing the state of the operation such as wether it was succesful or not and even if authentication is required. Some common response codes are:
* 200 OK
* 400 Bad Request
* 404 Not Found
* 500 Server Error

The API defines an uniform architecture that can evolve appart from the project we use it in, separating the code and functionality from the clients and servers.

The information about the session is managed different to that of a simple server architecture where the session is saved on the server side. A REST API is stateless and commonly uses Tokens to provide information about authentication, session validation, permissions and other information. The token architecture allows the API to keep a better track of the user's state and provides a better protection against attacks like session hijacking.

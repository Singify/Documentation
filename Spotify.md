# Spotify API

## Usage
The Spotify API will be used in the Singify Project for the following functions:

### APP
The users will be able to:
* Link their Spotify account
* Pick a song from their saved songs or playlists to be added to the karaoke queue

### BMS
The Karaoke administrator will be able to:
* Play the songs added to the sing karaoke queue by the app users
* Play and Pause the song


## HTTP Accepted Methods
The Spotify API accepts different HTTP Methods depending on the action you want to do, the following list represents the methods that will be used on this app.

Method |  Usage
---|---
GET | Used for retrieving resources.
PUT | Used for changing/replacing resources or collections. 

For this project, only the `GET` method will be needed since the user's library or information will not be modified in any way.

## HTTP Response Codes
Each API resource returns the result with a different HTTP response code depending on what happened.

Code | Message | Meaning
-----|---------|--------
200 | OK | The request has succeeded. The client can read the result of the request in the body and the headers of the response.
201 | Created | The request has been fulfilled and resulted in a new resource being created.
202 | Accepted |The request has been accepted for processing, but the processing has not been completed.
204 | No Content | The request has succeeded but returns no message body.
304 | Not Modified | See Conditional requests.
400 | Bad Request | The request could not be understood by the server due to malformed syntax. The message body will contain more information; see Error Details.
401 | Unauthorized | The request requires user authentication or, if the request included authorization credentials, authorization has been refused for those credentials.
403 | Forbidden | The server understood the request, but is refusing to fulfill it.
404 | Not Found | The requested resource could not be found. This error can be due to a temporary or permanent condition.
429 | Too Many Requests | Rate limiting has been applied.
500 | Internal Server Error | You should never receive this error because our clever coders catch them all ... but if you are unlucky enough to get one, please report it to us through a comment at the bottom of this page.
502 | Bad Gateway | The server was acting as a gateway or proxy and received an invalid response from the upstream server.
503 | Service Unavailable | The server is currently unable to handle the request due to a temporary condition which will be alleviated after some delay. You can choose to resend the request again.


## API Endpoints to Use
In order to achieve the usage intended for this API, the following API endpoints or resources will be used:

### App Endpoints:

Method | Endpoint | Usage
-------|----------|------
GET | `/v1/me` | [Get current user's profile](https://developer.spotify.com/web-api/get-current-users-profile/)
GET | `/v1/me/playlists` | [Get a list of the current user's playlists ](https://developer.spotify.com/web-api/get-a-list-of-current-users-playlists/)
GET | `/v1/users/{user_id}/playlists/{playlist_id}/tracks` | [Get a playlist's tracks](https://developer.spotify.com/web-api/get-playlists-tracks/)
GET | `/v1/me/tracks` | [Get user's saved tracks](https://developer.spotify.com/web-api/get-users-saved-tracks/)

### BMS Endpoints:

Method | Endpoint | Usage
-------|----------|------
GET | `/v1/tracks/{id}` | [Get a track](https://developer.spotify.com/web-api/get-track/)
PUT | `/v1/me/player/play` | [Start/Resume a User’s Playback](https://developer.spotify.com/web-api/start-a-users-playback/)
PUT | `/v1/me/player/pause` | [Pause a User’s Playback](https://developer.spotify.com/web-api/pause-a-users-playback/)

## Authorization
To use the Spotify API, proper authorization via OAuth must be done.

Method | Endpoint 
-------|---------
GET | `/authorize`

Required Information:
* Athorization URL: https://accounts.spotify.com/authorize
* Access Token URL: https://accounts.spotify.com/api/token
* Client ID: Obtained when the App is created
* Client Secret: Obtained when the App is created
* Redirect URL: Where we want to redirect after the authorization is made
* scope: `playlist-read-private playlist-read-collaborative user-read-playback-state user-modify-playback-state user-library-read`

The authorization will return a Token which will be sent with in the other requests as a way to provide the authentication of the request. Each token is only valid for a certain amount of time and must be refreshed when it becomes invalid.
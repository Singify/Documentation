# Singify API
## Usage
The Singify API will be used for the following functions:

### App
The users will be able to:
* Read the menu
* Place Orders
* Queue a song to sing
* Create and read reservations

#### Class Diagram

![Class Diagram](class-diagram.jpg)

### BMS
The Karaoke administrator will be able to:
* Read and modify the menu
* Read the Orders
* Read reservations


## HTTP Accepted Methods
The Singify API accepts different HTTP Methods depending on the action you want to do, the following list represents the methods that will be used on this app.

Method |  Usage
---|---
GET | Used for retrieving resources.
POST | Used to create resources.
PUT | Used to update or modify resources.
DELETE | Used to delete resources.


## HTTP Response Codes
Each API resource returns the result with a different HTTP response code depending on what happened.

Code | Message | Meaning
-----|---------|--------
200 | OK | The action was correctly processed
400 | Bad Request | The request was malformed, probably a missing parameter.
401 | Authentication Required | The user needs to be authenticated before proceeding to the resource.
403 | Forbidden | The user is accessing forbidden resources.
500 | Internal Server Error | An error happened on the server error

## API Endpoints to Use
In order to achieve the usage intended for this API, the following API endpoints or resources will be used:

###  Queue Endpoints:

Used By | Method | Endpoint | Usage
--------|--------|----------|------
App | PUT | `/queue/add` | Add a new song to the queue

###  Menu Endpoints:

#### Snacks

Used By | Method | Endpoint | Usage
--------|--------|----------|------
App/BMS | GET | `/snacks` | Get the list of all available snacks
App/BMS | GET | `/snack/{snack_id}` | Get information of a particular snack
BMS | POST | `/snack` | Register a new Snack
BMS | PUT | `/snack/{snack_id}` | Update a Snack's information
BMS | DELETE | `/snack/{snack_id}` | Delete a Snack from the menu

#### Food

Used By | Method | Endpoint | Usage
--------|--------|----------|------
App/BMS | GET | `/food` | Get the list of all available food
App/BMS | GET | `/food/{food_id}` | Get information of a particular food
BMS | POST | `/food` | Register a new Food
BMS | PUT | `/food/{food_id}` | Update a Food's information
BMS | DELETE | `/food/{food_id}` | Delete a Food from the menu

#### Drinks

Used By | Method | Endpoint | Usage
--------|--------|----------|------
App/BMS | GET | `/drinks` | Get the list of all available drinks
App/BMS | GET | `/drink/{drink_id}` | Get information of a particular drink
BMS | POST | `/drink` | Register a new Drink
BMS | PUT | `/drink/{drink_id}` | Update a Drink's information
BMS | DELETE | `/drink/{drink_id}` | Delete a Drink from the menu


###  Order Endpoints:

Used By | Method | Endpoint | Usage
--------|--------|----------|------
App/BMS | GET | `/orders/{user_id}` | Get the list of the user's orders
App/BMS | POST | `/order/{item_type}/{item_id}` | Place an Order


### Reservations Endpoints:
Used By | Method | Endpoint | Usage
--------|--------|----------|------
BMS | GET | `/reservations` | List all the registered reservations
App/BMS | GET | `/reservation/{reservation_id}` | Get information about a reservation, including the bill.
App | POST | `/reservation` | Create a new Reservation
App/BMS | DELETE | `/reservation/{reservation_id}` | Delete/Cancel a reservation


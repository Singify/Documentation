# Facebook API


## Usage
The Facebook [Graph API](https://developers.facebook.com/docs/graph-api/) will be used to manage user logins. The [Facebook SDK](https://developers.facebook.com/docs/javascript) may be used.

### App
The users will be able to:
* Login using Facebook
* Make a Karaoke Reservation with an optional linked facebook event

### BMS
The BMS will have no interaction with Facebook's APIs since it will not manage the events but the reservations created.

## Needed Permissions:
* public_profile
* email
* user_events

## Facebook JavaScript SDK Usage

### Loading
To load the SDK into our project, you must place the following code in your page:

```javascript
(function (d, s, id) {
	var js;
	var fjs = d.getElementsByTagName (s)[0];
	if (d.getElementById (id)) {
		return;
	}
	js = d.createElement(s);
	js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore (js, fjs);
} (document, "script", "facebook-jssdk"));
```

Then, you'll need to initialize it as follows:

```javascript
window.fbAsyncInit = function() {
	FB.init ({
		appId: 'your-app-id',
		autoLogAppEvents: true,
		xfbml: true,
		version: 'v2.10'
	});
};
```

### Login
```javascript
FB.login ({
	perms:'list_of_needed_permissions'
});
```

## Graph API Usage
Host: https://graph.facebook.com

### HTTP Response Codes
Code | Meaning
-----|--------
100 | Invalid parameter
200 | Permissions error

### Endpoints
Method | Endpoint | Usage
-------|----------|------
GET | `https://www.facebook.com/v2.10/dialog/oauth?client_id={app-id}&redirect_uri={redirect-uri}` | [User log in](https://developers.facebook.com/docs/facebook-login/manually-build-a-login-flow)
GET | `/v2.10/{user-id}/events` | [Get the user event list](https://developers.facebook.com/docs/graph-api/reference/user/events/)
GET | `/v2.10/{event-id}` | [Get Event information](https://developers.facebook.com/docs/graph-api/reference/event)

## Useful Links
* [Facebook Login for the Web with the JavaScript SDK](https://developers.facebook.com/docs/facebook-login/web)
* [Facebook authentication in your AngularJS web app](https://blog.brunoscopelliti.com/facebook-authentication-in-your-angularjs-web-app/)
* [Manually Creating a Login Workflow](https://developers.facebook.com/docs/facebook-login/manually-build-a-login-flow)




